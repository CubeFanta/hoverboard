const board = document.getElementById('board');
const colors = ['#6ebabae6', '#22733de6', '#87ab38a9', '#78636ca9', '#7dbdbda9'];
const SQUARES_NUMBER = 552;

for (let i = 0; i < SQUARES_NUMBER; i++) {
    const square = document.createElement('div')
    square.classList.add('square')

    square.addEventListener('mouseover', () => {
        setColor(square)
    })
    square.addEventListener('mouseleave', () => {
        removeColor(square)
    })
    board.addEventListener('mouseup', () => {
        setElement(square)
    })

    board.append(square)
}

function setColor(element) {
    const color = getRandomColor()
    element.style.backgroundColor = color
    element.style.boxShadow = `0 0 10px ${color}, 0 0 10px ${color}`
}
function removeColor(element) {
    element.style.transform = 'scale(0, 0)'
}
function getRandomColor() {
    const index = Math.floor(Math.random() * colors.length)
    return colors[index]
}
function setElement(element) {
    if (element.style.transform = 'scale(0, 0)') {
        element.style.transform = 'scale(1, 1)'
        element.style.backgroundColor = '#66996582'
        element.style.boxShadow = `0 0 2px #000`
    }
}